import React, { Component } from "react";
import { MDBContainer, MDBRow, MDBCol } from 'mdbreact';

export default class KapasitasMesin extends Component {
    state = {
        kapasitasMesin: 0,
        panjangLangkah: 0,
        diameterPiston: 0
    }
    hitung = (e) => {
        this.setState({
            [e.target.name]: e.target.value,
        }, () => {
            let kapasitasMesin = 0.785 * (this.state.diameterPiston * this.state.diameterPiston) * this.state.panjangLangkah / 1000
            this.setState({
                kapasitasMesin: kapasitasMesin
            })
        });
    };
    render() {
        return (
            <MDBContainer>
                <MDBRow>
                    <MDBCol md="12">
                        <form>
                            <label htmlFor="panjangLangkah" className="grey-text">
                                Panjang Langkah
                            </label>
                            <input onChange={this.hitung} type="number" id="panjangLangkah" className="form-control" name="panjangLangkah" />
                            <br />
                            <label htmlFor="diameterPiston" className="grey-text">
                                Diameter Piston
                            </label>
                            <input onChange={this.hitung} type="number" id="diameterPiston" className="form-control" name="diameterPiston" />
                            <h2 className='h1-responsive m-2 text-success'>{parseFloat(this.state.kapasitasMesin).toFixed(2)}</h2>
                        </form>
                    </MDBCol>
                </MDBRow>
            </MDBContainer >
        );
    }

};
