import React, { Component } from "react";
import { MDBCol, MDBContainer, MDBRow, MDBCard, MDBCardBody, MDBCardImage, MDBCardTitle, MDBCardText, MDBIcon } from "mdbreact";
import "./index.css";

class App extends Component {
  state = {
    kapasitasMesin: 0,
    panjangLangkah: 0,
    diameterPiston: 0,
    rasioKompresiStatis: 0,
    volumeDratBusi: 0,
    volumeRuangBakar: 0,
    kapasitasMesinCc: 0,
    langkahpiston: 0,
    convertLangkahPiston: 0,
    convertDiameterPiston: 0,
    nilai: 0,

  }
  kapasitasMesin = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    }, () => {
      let kapasitasMesin = 0.785 * (this.state.diameterPiston * this.state.diameterPiston) * this.state.panjangLangkah / 1000
      this.setState({
        kapasitasMesin: kapasitasMesin
      })
    });
  };
  rasioKompresiStatis = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    }, () => {
      let rasioKompresiStatis = (parseFloat(this.state.kapasitasMesinCc) + parseFloat(this.state.volumeRuangBakar) - parseFloat(this.state.volumeDratBusi)) / (parseFloat(this.state.volumeRuangBakar) - parseFloat(this.state.volumeDratBusi))
      this.setState({
        rasioKompresiStatis: rasioKompresiStatis
      })
    });
  };
  rasioKompresiDinamis = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    }, () => {
      let convertDiameterPiston = this.state.diameterPiston / 10
      let convertLangkahPiston = this.state.langkahPiston / 10
      let nilai = convertDiameterPiston * convertDiameterPiston * convertLangkahPiston * 0.785
      let nilai2 = parseFloat(this.state.volumeRuangBakar) - parseFloat(this.state.volumeDratBusi)
      let rasioKompresiDinamis =
        (nilai + nilai2) / nilai2
      this.setState({
        convertLangkahPiston: convertLangkahPiston,
        convertDiameterPiston: convertDiameterPiston,
        nilai: nilai,
        rasioKompresiDinamis: rasioKompresiDinamis
      })
    });
  };
  render() {
    return (
      <MDBContainer className="p-3">
        <MDBRow center style={{ height: "100vh" }}>
          <MDBCol middle={true} md='4'>
            <MDBCard narrow>
              <MDBCardImage
                className='view view-cascade gradient-card-header purple-gradient'
                cascade
                tag='div'
              >
                <h2 className='h2-responsive'>Kapasitas Mesin</h2>


              </MDBCardImage>
              <MDBCardBody cascade className='text-center'>
                <MDBContainer>
                  <MDBRow>
                    <MDBCol md="12">
                      <form>
                        <label htmlFor="panjangLangkah" className="grey-text">
                          Panjang Langkah
                        </label>
                        <input onChange={this.kapasitasMesin} type="number" id="panjangLangkah" className="form-control" name="panjangLangkah" />
                        <br />
                        <label htmlFor="diameterPiston" className="grey-text">
                          Diameter Piston
                        </label>
                        <input onChange={this.kapasitasMesin} type="number" id="diameterPiston" className="form-control" name="diameterPiston" />
                        <h2 className='h1-responsive m-2 text-success'>{parseFloat(this.state.kapasitasMesin).toFixed(2)}</h2>
                      </form>
                    </MDBCol>
                  </MDBRow>
                </MDBContainer >
              </MDBCardBody>
            </MDBCard>
          </MDBCol>
          <MDBCol middle={true} md='4'>
            <MDBCard narrow>
              <MDBCardImage
                className='view view-cascade gradient-card-header purple-gradient'
                cascade
                tag='div'
              >
                <h2 className='h3-responsive'>Rasio Kompresi Statis</h2>
              </MDBCardImage>
              <MDBCardBody cascade className='text-center'>
                <MDBContainer>
                  <MDBRow>
                    <MDBCol md="12">
                      <form>
                        <label
                          htmlFor="volumeDratBusi"
                          className="grey-text">
                          Volume Drat Busi
                        </label>
                        <input
                          onChange={this.rasioKompresiStatis}
                          type="number"
                          id="volumeDratBusi"
                          className="form-control"
                          name="volumeDratBusi" />
                        <br />
                        <label htmlFor="volumeRuangBakar" className="grey-text">
                          Volume Ruang Bakar
                        </label>
                        <input
                          onChange={this.rasioKompresiStatis}
                          type="number"
                          id="volumeRuangBakar"
                          className="form-control"
                          name="volumeRuangBakar" />
                        <br />
                        <label htmlFor="kapasitasMesinCc" className="grey-text">
                          Kapasitas Mesin (CC)
                        </label>
                        <input
                          onChange={this.rasioKompresiStatis}
                          type="number"
                          id="kapasitasMesinCc"
                          className="form-control"
                          name="kapasitasMesinCc" />
                        <h2 className='h1-responsive m-2 text-success'>{parseFloat(this.state.rasioKompresiStatis).toFixed(2)}</h2>
                      </form>
                    </MDBCol>
                  </MDBRow>
                </MDBContainer >
              </MDBCardBody>
            </MDBCard>
          </MDBCol>
          <MDBCol middle={true} md='4'>
            <MDBCard narrow>
              <MDBCardImage
                className='view view-cascade gradient-card-header purple-gradient'
                cascade
                tag='div'
              >
                <h2 className='h4-responsive '>Rasio Kompresi Dinamis</h2>


              </MDBCardImage>
              <MDBCardBody cascade className='text-center'>
                <MDBContainer>
                  <MDBRow>
                    <MDBCol md="12">
                      <form>

                        <label htmlFor="langkahPiston"
                          className="grey-text">
                          LANGKAH PISTON BERADA DI TITIK SENTUH 0.01mm KLEP MENUTUP SEMPURNA
                        </label>
                        <input
                          onChange={this.rasioKompresiDinamis}
                          type="number"
                          id="langkahPiston"
                          className="form-control"
                          name="langkahPiston" />
                        <br />
                        {/* <label htmlFor="kapasitasMesinCc" className="grey-text">
                          KONFERT LANGKAH PINTON TMB KE TMA
                          : <h2 className='h4-responsive m-2 text-success d-inline'>{parseFloat(this.state.convertLangkahPiston).toFixed(2)}</h2>
                        </label>
                        <label htmlFor="kapasitasMesinCc" className="grey-text">
                          NILAI CONFERT DIAMETER PISTON (mm-CM)
                          : <h2 className='h4-responsive m-2 text-success d-inline'>{parseFloat(this.state.convertDiameterPiston).toFixed(3)}</h2>
                        </label>
                        <label htmlFor="kapasitasMesinCc" className="grey-text">
                          NILAI
                          : <h2 className='h4-responsive m-2 text-success d-inline'>{parseFloat(this.state.nilai).toFixed(3)}</h2>
                        </label> */}
                        <h2 className='h1-responsive m-2 text-success'>{parseFloat(this.state.rasioKompresiDinamis).toFixed(2)}</h2>
                      </form>
                    </MDBCol>
                  </MDBRow>
                </MDBContainer >
              </MDBCardBody>
            </MDBCard>
          </MDBCol>
        </MDBRow>
      </MDBContainer>
    );
  }
}

export default App;
